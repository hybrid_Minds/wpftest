﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using WpfTest.Navigation;
using WpfTest.ViewModels;

namespace WpfTest.Model
{
    class SomeSource
    {
        DispatcherFrame _frame = new DispatcherFrame();

        public string GetData(EditViewModel editViewModel)
        {
            NavigationService.NavigateToEditView();
            WaitForCommit(editViewModel);
            NavigationService.NavigateToInfoView();
            return editViewModel.Data;
        }

        private void WaitForCommit(EditViewModel editViewModel)
        {
            editViewModel.Commited += Continue;
            Dispatcher.PushFrame(_frame);
            editViewModel.Commited -= Continue;
        }

        private void Continue()
        {
            _frame.Continue = false;
        }
    }
}
