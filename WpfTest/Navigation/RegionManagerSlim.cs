﻿using System;
using System.Collections.Concurrent;
using System.Windows.Controls;

namespace WpfTest.Navigation
{
    class RegionDetails
    {
        private readonly Action<UserControl> _changeView;
        private static ConcurrentDictionary<string, UserControl> _views =
            new ConcurrentDictionary<string, UserControl>();

        public RegionDetails(Action<UserControl> changeView)
        {
            _changeView = changeView;
        }

        public void AddView(string name, UserControl view)
        {
            if(_views.ContainsKey(name)) throw new InvalidOperationException($"region is already contain view name: {name}");
            _views.TryAdd(name, view);
        }

        public void ChangeView(string name)
        {
            if (!_views.ContainsKey(name)) throw new InvalidOperationException($"region does not contain view name: {name}");
            var view = _views[name];
            _changeView.Invoke(view);
        }
    }
    public class RegionManagerSlim
    {
        private static ConcurrentDictionary<string, RegionDetails> _regions =
            new ConcurrentDictionary<string, RegionDetails>();
        public static void AddRegion(string name, Action<UserControl> changeView)
        {
            if (_regions.ContainsKey(name))
                throw new InvalidOperationException($"region manager already contains region name: {name}");
            var regionDetails = new RegionDetails(changeView);
            _regions.TryAdd(name, regionDetails);
        }

        public static void AddView(string regionName, string viewName, UserControl view)
        {
            if (!_regions.ContainsKey(regionName)) throw new InvalidOperationException($"region manager does not contain region name: {regionName}");
            _regions[regionName].AddView(viewName, view);
        }
        public static void ChangeView(string regionName, string viewName)
        {
            if(!_regions.ContainsKey(regionName)) throw new InvalidOperationException($"region manager does not contain region name: {regionName}");
            _regions[regionName].ChangeView(viewName);
            
        }
    }
}
