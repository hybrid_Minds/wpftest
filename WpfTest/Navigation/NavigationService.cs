﻿using WpfTest.Views;

namespace WpfTest.Navigation
{
    public static class NavigationService
    {
        private const string InfoView = "InfoView";
        private const string EditView = "EditView";

        static NavigationService()
        {
            RegionManagerSlim.AddView(RegionNames.Main, InfoView, new InfoView());
            RegionManagerSlim.AddView(RegionNames.Main, EditView, new EditView());
        }

        public static void NavigateToInfoView()
        {
            RegionManagerSlim.ChangeView(RegionNames.Main, InfoView);
        }

        public static void NavigateToEditView()
        {
            RegionManagerSlim.ChangeView(RegionNames.Main, EditView);
        }
    }
}
