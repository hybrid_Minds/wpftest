﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WpfTest.Infrastructure;
using WpfTest.Model;

namespace WpfTest.ViewModels
{
    public class EditViewModel : ViewModelBase
    {
        private string _data;

        public EditViewModel()
        {
            BindCommands();
        }

        public event Action Commited;

        public ICommand Commit { get; protected set; }

        public string Data
        {
            get { return _data; }
            set
            {
                _data = value;
                OnPropertyChanged("Data");
            }
        }

        private void BindCommands()
        {
            Commit = new RelayCommand(OnCommit);
        }

        private void OnCommit()
        {
            var commited = Commited;
            commited?.Invoke();
        }
    }
}
