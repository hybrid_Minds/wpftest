﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using WpfTest.Infrastructure;
using WpfTest.Model;
using WpfTest.Navigation;
using WpfTest.ViewModels;

namespace WpfTest.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        private string _data;

        public MainViewModel(EditViewModel editViewModel)
        {
            EditViewModel = editViewModel;
        }

        public EditViewModel EditViewModel { get; }

        public string Data
        {
            get { return _data; }
            set
            {
                _data = value;
                OnPropertyChanged("Data");
            }
        }

        public ICommand GetDataCommand
        {
            get
            {
                return new RelayCommand(() =>
                {
                    var ds = new SomeSource();
                    Data = ds.GetData(EditViewModel);
                });
            }
        }
    }
}
