﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfTest.Navigation;
using WpfTest.ViewModels;

namespace WpfTest
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            var window = new MainWindow();
            var editViewModel = new EditViewModel();
            var viewModel = new MainViewModel(editViewModel);
            window.SetDataContext(viewModel);
            //setting default view
            NavigationService.NavigateToInfoView();
            App app = new App();
            app.Run(window);
        }
    }
}
