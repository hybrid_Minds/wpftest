﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfTest.Navigation;

namespace WpfTest.Controls
{
    /// <summary>
    /// Interaction logic for RegionSlim.xaml
    /// </summary>
    public partial class RegionSlim : UserControl
    {
        public static DependencyProperty RegionNameProperty =
            DependencyProperty.Register("RegionName", typeof(string), typeof(RegionSlim));

        public RegionSlim()
        {
            InitializeComponent();

            var propertyDescriptor = DependencyPropertyDescriptor.FromProperty(RegionNameProperty, typeof(RegionSlim));
            propertyDescriptor.AddValueChanged(this, Handler);
        }

        private void Handler(object sender, EventArgs eventArgs)
        {
            RegionManagerSlim.AddRegion(RegionName, v => this.contentControl.Content = v);
        }

        public string RegionName
        {
            get { return (string)GetValue(RegionNameProperty); }
            set { SetValue(RegionNameProperty, value); }
        }
    }
}
